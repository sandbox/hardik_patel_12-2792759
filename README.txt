INTRODUCTION
------------
Find PHP Code written in Views. This module will gone show you a table in which
you will find name of view's and PHP code written in  Global PHP Field.
This module will  Create views for it (link will be provided on the top of
table. 
Using this view you can add additional functionality to this views).


For a full description of the module, visit the project page:
  https://www.drupal.org/sandbox/hardik_patel_12/2792759

To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/user


REQUIREMENTS
------------

  This module requires the following modules:

 * Views (https://drupal.org/project/views)
 * Views PHP (https://www.drupal.org/project/views_php)


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.
* You may want to disable Toolbar module, since its output clashes with
   Administration Menu.


CONFIGURATION
-------------
 
 * Configure user permissions in Administration » People » Permissions:

   - Use the administration pages and help (System module)
   - Set permission (Administrator php code in views) in permission page
     according to your requirements.

   - Access  menu
   -This module create link (Find Php Code) in navigation menu .
    Go to Administration » Structure »
    Menus » Navigation menu. here you will find Find Php Code Menu Link.


TROUBLESHOOTING
---------------

 * If the menu does not display, check the following:

  - Are the "Access administration menu" and "Use the administration pages
    and help" permissions enabled for the appropriate roles?


FAQ
---
Q: Why use this module?

A: This module will helpful for developer who are working on large project.
Which involve numbers of views in their project now  .
if they want to find php code written in views this module will help them.


MAINTAINERS
-----------
Current maintainers:
* Hardik Patel - https://www.drupal.org/user/3316709/
